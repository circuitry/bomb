#include <Arduino.h>
#include <FastLED.h>
#include "callbacks.h"
#include "pins.h"

static uint8_t led_value = 0L;
static const int LED_COUNT = 8;
static CRGB leds[LED_COUNT];

void led_strip_task_setup() {
  pinMode(LED_STRIP_BUTTON1_PIN, INPUT_PULLUP);
  pinMode(LED_STRIP_BUTTON2_PIN, INPUT_PULLUP);
  pinMode(LED_STRIP_BUTTON3_PIN, INPUT_PULLUP);
  pinMode(LED_STRIP_BUTTON4_PIN, INPUT_PULLUP);

  FastLED.addLeds<NEOPIXEL, LED_STRIP_PIN>(leds, LED_COUNT);
  FastLED.setBrightness(20);
  led_value = random(255);
}

void read_buttons() {
  if (digitalRead(LED_STRIP_BUTTON1_PIN) == LOW) {
    led_value += 1;
    delay(200);
  }
  if (digitalRead(LED_STRIP_BUTTON2_PIN) == LOW) {
    led_value |= 1;
    delay(200);
  }
  if (digitalRead(LED_STRIP_BUTTON3_PIN) == LOW) {
    led_value = ~led_value;
    if (led_value == 255) led_value = 1;
    delay(200);
  }
  if (digitalRead(LED_STRIP_BUTTON4_PIN) == LOW) {
    led_value = led_value << 1;
    if (led_value == 0) led_value = 1;
    delay(200);
  }
}

void led_strip_callback() {
  static bool init_done = false;
  if (!init_done) {
    led_strip_task_setup();
    init_done = true;
  }

  for (int i = 0; i < LED_COUNT; i++) {
    if ((led_value & (1 << i)) != 0)
      leds[i] = CRGB(0xff0000);
    else
      leds[i] = CRGB(0);
  }
  if ((led_value & 0xff) == 0xff) {
    for (int i = 0; i < LED_COUNT; i++) {
      leds[i] = CRGB(0x00ff00);
      FastLED.show();
      delay(200);
      stage_task.setCallback(morse_code_callback);
    }
  }
  read_buttons();
  FastLED.show();
}
