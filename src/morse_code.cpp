#include <Arduino.h>
#include "callbacks.h"
#include "pins.h"

static const uint8_t MESSAGE_COUNT = 5;
static String messages[MESSAGE_COUNT] = {"SOS", "ABC", "XXX", "BOOM", "SMS"};
static String morse_code[] = {
    ".-",   "-...", "-.-.", "-..",  ".",   "..-.", "--.",  "...",  "..",
    ".---", "-.-",  ".-..", "--",   "-.",  "---",  ".--.", "--.-", ".-.",
    "...",  "-",    "..-",  "...-", ".--", "-..-", "-.--", "--.."};

String to_morse(String text) {
  String result = "";
  for (char c : text) {
    String morse = morse_code[c - 'A'];
    for (char c : morse) {
      if (c == '.')
        result = result + "100";
      else if (c == '-')
        result = result + "111100";
    }
    result = result + "00000000";
  }
  return result;
}

void morse_code_callback() {
  static auto message = to_morse(messages[random(MESSAGE_COUNT)]);
  static auto pos = 0;
  static bool init = false;

  if (!init) {
    pinMode(MORSE_CODE_PIN, OUTPUT);
    init = true;
  }
  auto c = message[pos];
  Serial.println("Message " + message + ", now at " + c);
  if (c == '1') {
    digitalWrite(MORSE_CODE_PIN, HIGH);
    stage_task.delay(100);
  } else {
    digitalWrite(MORSE_CODE_PIN, LOW);
    stage_task.delay(100);
  }
  pos++;
  if (pos == message.length()) {
    digitalWrite(MORSE_CODE_PIN, LOW);
    stage_task.setCallback(done_callback);
  }
}
