#include <Arduino.h>
#include "callbacks.h"
#include "pins.h"

void countdown_callback() {
  static long seconds = 0;
  static bool init = false;
  if (!init) {
    init = true;
    pinMode(DISPLAY_PIN, OUTPUT);
  }
  digitalWrite(DISPLAY_PIN, seconds % 2 == 0);
  seconds++;
}
