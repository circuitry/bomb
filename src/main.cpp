#include <Arduino.h>
#include <TM1637Display.h>
#include <TaskScheduler.h>
#include "callbacks.h"
#include "pins.h"

Scheduler ts;

static Task stage_task(50L, TASK_FOREVER, &morse_code_callback, &ts, false);
static Task timer_task(500L, TASK_FOREVER, &countdown_callback, &ts, true);

void setup() {
  randomSeed(analogRead(0));
  Serial.begin(115200);
  stage_task.enable();
}

void loop() { ts.execute(); }
